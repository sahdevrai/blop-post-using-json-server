import './App.css';
import Home from './components/home/Home.component';
import Contact from './components/contact/Contact.component'
import About from './components/about/About.component'
import {Routes, Route} from 'react-router-dom'
import Blog from './components/blog/Blog.component';
import EditBlog from './components/editBlog/EditBlog.component';


function App() {
  
  return (
   <Routes>
     <Route path='/' index element={<Home/>}></Route>
     <Route path='/home' element={<Home/>}></Route>
     <Route path='/about' element={<About/>}></Route>
     <Route path='/home/about' element={<About/>}></Route>

     <Route path='/contact' element={<Contact/>}></Route>
     <Route path='/home/contact' element={<Contact/>}></Route>
     <Route path='/home/blog/:id' element={<Blog/>}></Route>
     <Route path='/blog/:id' element={<Blog/>}></Route>

     <Route path='/home/blog/edit/:id' element={<EditBlog/>}></Route>
     <Route path='/blog/edit/:id' element={<EditBlog/>}></Route>

   </Routes>
  );
}

export default App;
