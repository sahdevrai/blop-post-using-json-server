import React from 'react'
import Header from '../header/Header.component'
import './about.css'

const About=()=>{
    return(
    <>
    <Header/>
    <div className="display-4 dis">
        About us
    </div>
    <div className="lead" style={{"marginLeft":"5%"}}>
    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Asperiores repudiandae delectus tempore placeat animi repellendus eum doloribus quaerat? Reprehenderit alias optio voluptatem quas eligendi ex sint veniam, harum quidem cumque.
    </div>
    </>
    )
}

export default About;