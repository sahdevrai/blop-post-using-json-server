import React,{useEffect,useState} from 'react'
import Header from '../header/Header.component'
import './editBlog.css';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

const EditBlog=()=>{
    const [blog, setBlog] = useState([])
    const [fame, setFame] = useState("")

    const [name, setName] = useState("")
    const [title, setTitle] = useState("")
    const [disc, setDisc] = useState("")
    const [content, setContent] = useState("")
    const navigate=useNavigate()

    useEffect(() => {
        var x=window.location.pathname
  
          x=x.split('/')
  
        axios.get(`http://localhost:8000/people/${x[3]}`)
        .then(Response=>{
          setBlog(Response.data)
          setName(blog.authorName)
          setTitle(blog.title)
          setDisc(blog.discription)
          setContent(blog.blog)
          setFame(blog.id)
          console.log(Response.data)
        })
      },[fame])

      const submit=()=>{
        var x=window.location.pathname
  
        x=x.split('/')
        axios.put(`http://localhost:8000/people/${x[3]}`, {
            title: title,
            authorName:name,
            blog:content,
            discription:disc
        });
        navigate("/")
      }

    return(
    <>
    <Header/>
    <div className="display-4 head">
        Edit Your Blog 
    </div>
    <div className='content'>
       
    <form>
        <div className='form in' >
        <div className="col-4 col1">
        <label>Title</label>
      <input type="text" className="form-control" value={title} onChange={(e)=>{setTitle(e.target.value)}} placeholder="Title"/>
    </div>
    <div className="col-4 col2">
    <label>Author Name</label>
      <input type="text" className="form-control" value={name} onChange={(e)=>{setName(e.target.value)}} placeholder="Author Name"/>
    </div>
    </div>
    <div className='col-8 in'>
    <label>Description</label>
      <input type="text" className="form-control" value={disc} onChange={(e)=>{setDisc(e.target.value)}} placeholder="Discription"/>
    </div>
    <div className="form-group col-8 in">
    <label for="exampleFormControlTextarea1">Example textarea</label>
    <textarea className="form-control" id="exampleFormControlTextarea1" value={content} onChange={(e)=>{setContent(e.target.value)}} rows="3"></textarea>
  </div>
</form>
    </div>
    <div type="submit" class="btnforsrch1" onClick={()=>{submit()}}>Submit</div>

    </>
    )
}

export default EditBlog;