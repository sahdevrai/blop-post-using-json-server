import React,{useEffect,useState} from 'react'
import Header from '../header/Header.component'
import './blog.css'
import axios from 'axios'

const Blog=()=>{
    const [blog, setBlog] = useState([])
    
    useEffect(() => {
      var x=window.location.pathname

        x=x.split('/')

      axios.get(`http://localhost:8000/people/${x[2]}`)
      .then(Response=>{
        setBlog(Response.data)
        console.log(Response.data)
      })
    },[])
    return(
    <>
    <Header/>
       <div className='display-2 dp2'>
        {blog.title} 
      </div>
      <div className='disc'>
      <p className='discription'>{blog.discription}
      </p>
      <p className='lead ld'>by {blog.authorName}</p>
      </div>
      <div className='blog'>
        {blog.blog}
      </div>
            
    </>
    )
}

export default Blog;