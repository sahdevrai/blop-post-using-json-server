import React,{useEffect,useState} from 'react'
import './home.css'
import axios from 'axios'
import Header from '../header/Header.component'
import { useNavigate } from 'react-router-dom'

const Home=()=>{
const [blog, setBlog] = useState([])
const navigate=useNavigate();
    useEffect(() => {
      axios.get("http://localhost:8000/people")
      .then(Response=>{
        setBlog(Response.data)
        console.log(Response.data)
      })
    },[])
    
    const del=(id)=>{
        axios.delete(`http://localhost:8000/people/${id}`)
        .then(()=>{
            console.log("delete successful");
            window.location.reload();
        })
    }


    return(
    <>
    <Header/>
        <div className='display-2 headingblog'>
            Blogs
        </div>
    {

        blog.map((value)=>{
            return(
                <>
                    <div className="jumbotron jumbotron-fluid">
                    <div className="container">
                    <div className='row'>
                        <h1 className="display-5 col-7 dp4" onClick={()=>{navigate(`blog/${value.id}`)}} >{value.title}</h1>
                        
                        <div className='col'>
                            <button className='btn btn-primary btn1' onClick={()=>{navigate(`blog/edit/${value.id}`)}}><small>Edit</small> </button>
                        </div>
                        <div className='col'>
                            <button className='btn btn-primary btn2' onClick={()=>{navigate(`blog/${value.id}`)}}><small>Show</small></button>
                        </div>
                        <div className='col'>
                            <button className='btn btn-primary btn3' onClick={()=>{del(value.id)}}><small>Delete</small></button>
                        </div>
                        </div>
                        <p className="lead">{value.discription}</p>
                    </div>
                    </div>
                </>
                )
            })    
        }
    </>
    )
}

export default Home;