import React from 'react'
import ima from "./logo.svg" 
import './header.css'
import { Link } from 'react-router-dom'

const Header=()=>{

    return(
    <>
       <nav className="navbar navbar-expand-lg navbar-light bg-light">
           <img src={ima} alt="logo" width={"40px"} className="logo"/>
  <Link className="navbar-brand" to="/">Blogpost</Link>
  <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>

  <div className="collapse navbar-collapse" id="navbarSupportedContent">
    <ul className="navbar-nav mr-auto">
      <li className="nav-item active">
        <Link className="nav-link" to="/home">Home</Link>
      </li>
      <li className="nav-item">
        <Link className="nav-link" to="/contact">Contact</Link>
      </li>
      <li className="nav-item">
        <Link className="nav-link" to="/about">About</Link>
      </li>
      {/* <li className="nav-item dropdown">
        <a className="nav-link dropdown-toggle" href="#e" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dropdown
        </a>
        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
          <a className="dropdown-item" href="#e">Action</a>
          <a className="dropdown-item" href="#e">Another action</a>
          <div className="dropdown-divider"></div>
          <a className="dropdown-item" href="#e">Something else here</a>
        </div>
      </li> */}
      
    </ul>
    <form className="form-inline my-2 my-lg-0 search row">
        <div className='col'>
        <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"/>
        </div>
       {/* <btn className='btn btn-outline-success col srchbtn'>Submit</btn> */}
        {/* <div className='col'>
      <button className="btn btn-outline-success  srchbtn" type="submit">Search</button>
      </div> */}
      <div className="col">
        <div className='btnforsrch'>Search</div>
      </div>
    </form>
  </div>
</nav>

    </>
    )
}

export default Header;